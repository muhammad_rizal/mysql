1. create database myshop;

2.
 //2.1 
	MariaDB [(none)]> use myshop;
	Database changed
	MariaDB [myshop]> create table users (id int primary key, name varchar(255), email varchar(255),password varchar(255));
	MariaDB [myshop]> alter table users modify id int not null auto_increment;
//2.2
	 MariaDB [myshop]> create table categories (id int primary key, name varchar(255));
	MariaDB [myshop]> alter table categories modify id int not null auto_increment;
//2.3 
	MariaDB [myshop]> create table items (id int primary key, name varchar(255), description varchar(255), price integer, stock integer, category_id integer, foreign key (category_id) references categories (id) );
	MariaDB [myshop]> alter table items modify id int not null auto_increment;

3.
	//table users
	MariaDB [myshop]> insert into users (name, email, password)
    -> values ("John Doe","john@doe.com", "john123");
	MariaDB [myshop]> insert into users (name, email, password)
    -> values ("Jane doe","jane@doe.com", "jenita123");

 	//table categories
	MariaDB [myshop]> insert into categories (name)
    -> value("gadget");
	MariaDB [myshop]> insert into categories (name)
    -> value("cloth");
	MariaDB [myshop]>
	MariaDB [myshop]> insert into categories (name)
    -> value("men");
	Query OK, 1 row affected (0.039 sec)
	MariaDB [myshop]> insert into categories (name)
    -> value("women");
	MariaDB [myshop]> insert into categories (name)
    -> value("branded");

	//table items
	MariaDB [myshop]> insert into items (name, description, price, stock, category_id) values
    -> ("samsung b50", "hape keren dari merek samsung",4000000,100,1);
	MariaDB [myshop]> insert into items (name, description, price, stock, category_id) values
    -> ("Uniklooh", "baju keren dari brand ternama",500000,50,2);
	MariaDB [myshop]> insert into items (name, description, price, stock, category_id) values
    -> ("IMHO Watch", "jam tangan anak yang jujur banget",2000000,10,1);
	




4.
//a.
	MariaDB [myshop]> select id, name, email from users;
//b.
	MariaDB [myshop]> select * from items where price >1000000;
	MariaDB [myshop]> select * from items where name like "%watch";
//c.
	MariaDB [myshop]> select items.id, items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;
	

5. MariaDB [myshop]> update items set price = 2500000 where id = 1;
